# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v3.2.1-SNAPSHOT] - [2020-10-08]

adoption of gcube-smartgears-bom.2.0.0

